# Design WEBB

Website I did for the university course "Design För Användarupplevelse" at Umeå University.

The prerequisets for this site are that, t showcases a fictional outdoors clothing company, called "fritix". But also that it has 3 different Menu's containing:



- Menu A, 
    - containing both a Log in and a Sign in element;
    - Your Cart element;
    - Sizes;
    - Customer Support;
    - A search function;
    - A land picker.

- Menu B,
    - Sale icon;
    - Jackets;
    - Skirts;
    - Pants;
    - Beanies and Gloves;
    - Underwear;
    - Shoes and boots;
    - Camp (Tält);
    - Sleeping Bags;
    - Food;
    - Dog.

- Menu C,
    - Consumer agreements;
    - About us;
    - Work with us;
    - News;
    - Blog for us.

- Menu D,
    - Our Bloggers;
    - Instagram;
    - Facebook;